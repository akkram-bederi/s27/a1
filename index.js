const HTTP = require(`http`);
const PORT = 3003;


HTTP.createServer((req,res) => {
    if(req.url=="/" && req.method =="GET"){
        res.writeHead(200,{"Content-Type":"text/plain"});
        res.end("Welcome to Booking system")
    }else if (req.url=="/profile" && req.method =="GET"){
        res.writeHead(200,{"Content-Type":"text/plain"});
        res.end("Welcome to your Profile")
    }else if (req.url=="/courses" && req.method =="GET"){
        res.writeHead(200,{"Content-Type":"text/plain"});
        res.end("Here's our courses available")
    }else if (req.url=="/addCourse" && req.method =="POST"){
        res.writeHead(200,{"Content-Type":"text/plain"});
        res.end("Add a course to our resources")
    }else if (req.url=="/users" && req.method =="PUT"){
        res.writeHead(200,{"Content-Type":"text/plain"});
        res.end("Update a course to our resources")
    }else if (req.url=="/users" && req.method =="DELETE"){
        res.writeHead(200,{"Content-Type":"text/plain"});
        res.end("Archive courses to our resources")
    }
}).listen(PORT, ()=>console.log(`Server connected to port ${PORT}`));